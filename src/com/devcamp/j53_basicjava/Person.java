﻿package com.devcamp.j53_basicjava;

import java.util.Arrays;

public class Person extends Object {
    public String name;//ten
    public int age;//tuoi
    public double weight;//can nang
    public long salary;//thu nhap
    public String[] pets;//danh sach con vat    

    /*     * Ham khoi tao co 1 tham so     */
    public Person(String name) {
        this.name = name;
        this.age = 20;
        this.weight = 30;
        this.salary = 10000000;
        this.pets = new String[] {"Dog", "Cat", "Chicken"};
    }
    /**     * Ham khoi tao khong co tham so     */
    public Person() {
        this("Nguyen Van Nam");
    }
    /*     * Ham khoi tao co 3 tham so     */
    public Person(String name, int age, double weight) {
        this(name);
        this.age = age;
        this.weight = weight;
    }
    /*     * Ham khoi tao co tất cả 5 tham so     */
    public Person(String name, int age, double weight, long salary, String[] pets ) {
        this(name, age, weight);
        this.salary = salary; 
        this.pets = pets;
    }    

    /*     Phuong thuc hien thi du lieu    */
    public void showInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Weight: " + this.weight);
        System.out.println("Salary: " + this.salary);
        System.out.println("Pets: " + this.pets);
    }

    /*    Override: phuong thuc giong het lop cha (giong ca ten va tham so)     */
    @Override
    public String toString() {
        String strPerson = "{";
        strPerson += "Name: " + this.name + ", ";
        strPerson += "Age: " + this.age + ", ";
        strPerson += "Weight: " + this.weight + ", ";
        strPerson += "Salary: " + this.salary + ", ";
        strPerson += "Pets: " + Arrays.toString(pets);
        strPerson += "}";
        return strPerson;
    }
}
