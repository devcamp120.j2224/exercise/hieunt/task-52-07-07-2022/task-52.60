﻿package com.devcamp.j53_basicjava;
import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    int id; //id của order
    String customerName; //ten khach hang
    long price; //tổng tiền
    Date orderDate; //ngày thực hiện order
    boolean confirm; // xác nhận hay chưa
    String[] items; //danh sach mặt hàng đã mua    

    // Ham khoi tao co 1 tham so customerName
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"book", "pen", "ruler"};
    }
    // Ham khoi tao co tất cả 6 tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    } 
    // Ham khoi tao không co tham so 
    public Order() {
        this("Hieu");
    }
    // Ham khoi tao co 3 tham so
    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] {"book", "pen", "ruler"});
    }   

    /*    Override: phuong thuc giong het lop cha (giong ca ten va tham so)     */
    @Override
    public String toString() {
        //Tạo Định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi", "VN"));
        //Tạo định dạng ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //Tạo định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        return
            " Order {id: " + id
            + ", customerName: " + customerName
            + ", price: " + usNumberFormat.format(price)
            + ", orderDate: " + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm: " + confirm
            + ", items: " + Arrays.toString(items)
            + "}";
    }
}
