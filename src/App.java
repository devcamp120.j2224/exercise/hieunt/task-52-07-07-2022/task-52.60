import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j53_basicjava.Order2;
import com.devcamp.j53_basicjava.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> orderList = new ArrayList<Order2>();
        Order2 order21 = new Order2();
        Order2 order22 = new Order2("Lan");
        Order2 order23 = new Order2(3, "Long", 80000);
        Order2 order24 = new Order2(4, "Nam", 75000, new Date(),
            false, new String[] {"paper", "pen"}, new Person("Nam"));
 
        //Add cac order vao orderList
        orderList.add(order21);
        orderList.add(order22);
        orderList.add(order23);
        orderList.add(order24);
        
        //In ra màn hình
        /* gõ code này vào terminal để hiển thị ký tự đặc biệt
        $OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding =
                    New-Object System.Text.UTF8Encoding
        */
        try ( 
            PrintWriter consoleOut = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8)))
        {
            for(Order2 order2 : orderList) {
                System.out.println(order2.toString());
            }
        } 
    }
}
